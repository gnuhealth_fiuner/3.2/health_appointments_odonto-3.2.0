# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_appointments_odonto import *
from .report import *

def register():
    Pool.register(
        AppointmentReportOdonto,
        module='health_appointments_odonto_fiuner', type_='model')
    Pool.register(
        ReporteTurnosOdonto,
        module='health_appointments_odonto_fiuner', type_='report')
